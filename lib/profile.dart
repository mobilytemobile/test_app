import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:test_app/picker.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Picker picker = new Picker();
  Future<File> _imageFile;
  Color textHintColor = Color.fromRGBO(160, 160, 160, 1.0);
  Color textPhotoColor = Color.fromRGBO(160, 160, 160, 1.0);
  Color iconColor = Color.fromRGBO(199, 199, 204, 1.0);
  double screenWidth;

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    final name = AppTextField(
      hint: "Name",
      textColor: Colors.black,
      hintColor: textHintColor,
      underlineColor: textHintColor,
    );

    final type = AppTextField(
      hint: "Type (e.g. Dog, Cat)",
      textColor: Colors.black,
      hintColor: textHintColor,
      underlineColor: textHintColor,
    );

    final breed = AppTextField(
      hint: "Breed",
      textColor: Colors.black,
      hintColor: textHintColor,
      underlineColor: textHintColor,
    );
    final weight = AppTextField(
      hint: "Weight",
      textColor: Colors.black,
      hintColor: textHintColor,
      underlineColor: textHintColor,
    );
    final birthYear = AppTextField(
      hint: "Birth Year",
      textColor: Colors.black,
      hintColor: textHintColor,
      underlineColor: textHintColor,
    );

    return new Scaffold(
      appBar: getAppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            width: screenWidth,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _previewImage(),
                Container(
                  margin: EdgeInsets.only(top: 80.0, right: 80.0),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    "Photo",
                    style: TextStyle(
                      color: textPhotoColor,
                      fontSize: 20.0,
                      fontFamily: 'Amagh_Demo',
                    ),
                  ),
                ),
                Icon(
                  Icons.keyboard_arrow_right,
                  size: 48.0,
                  color: iconColor,
                ),
              ],
            ),
          ),
          SizedBox(height: 8.0),
          Container(
            height: 1.0,
            color: textHintColor,
            width: double.infinity,
          ),
          SizedBox(height: 4.0),
          name,
          SizedBox(height: 8.0),
          type,
          SizedBox(height: 8.0),
          breed,
          SizedBox(height: 8.0),
          weight,
          SizedBox(height: 8.0),
          birthYear,
        ],
      ),
    );
  }

  String image;

  Widget _previewImage() {
    return FutureBuilder<File>(
      future: _imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          image = snapshot.data.path;
        }
        return InkWell(
          child: Container(
            margin: EdgeInsets.only(top: 12.0),
            width: 120.0,
            height: 120.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.cover,
                image: (snapshot.connectionState == ConnectionState.done &&
                        snapshot.data != null
                    ? FileImage(snapshot.data)
                    : snapshot.error != null
                        ? AssetImage('assets/images/ic_profile_blank.png')
                        : image != null
                            ? CachedNetworkImageProvider(image)
                            : AssetImage('assets/images/ic_profile_blank.png')),
              ),
            ),
          ),
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Select Option"),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          setState(() {
                            _imageFile = picker.pickImage(ImageSource.camera);
                            if (_imageFile != null) {
                              textPhotoColor = Colors.black;
                            }
                          });
                        },
                        child: Text("Camera"),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          setState(() {
                            _imageFile = picker.pickImage(ImageSource.gallery);
                            if (_imageFile != null) {
                              textPhotoColor = Colors.black;
                            }
                          });
                        },
                        child: Text("Gallery"),
                      ),
                    ],
                  );
                });
          },
        );
      },
    );
  }

  AppBar getAppBar() {
    return AppBar(
      title: Text(
        "Pet Information",
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.black,
          fontFamily: 'Amagh_Demo',
        ),
      ),
      backgroundColor: Color.fromRGBO(238, 238, 238, 1.0),
      centerTitle: false,
      elevation: 0.0,
    );
    //return appBar;
  }
}

class AppTextField extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final onSaved;
  final validator;
  final Color underlineColor;
  final Color hintColor;
  final Color textColor;
  final TextInputType keyboardType;
  final bool showPasswordType;
  final bool enabled;
  final double fontSize;
  final int maxLength;
  final bool autoFocus;
  final TextAlign textAlign;
  final FocusNode focusNode;
  final Color cursorColor;
  final Color disabledColor;

  AppTextField(
      {this.hint,
      this.controller,
      this.onSaved,
      this.validator,
      this.hintColor = Colors.white,
      this.textColor = Colors.white,
      this.underlineColor = Colors.white,
      this.keyboardType = TextInputType.text,
      this.showPasswordType = false,
      this.enabled = true,
      this.autoFocus = false,
      this.fontSize = 20.0,
      this.maxLength,
      this.textAlign = TextAlign.start,
      this.focusNode,
      this.cursorColor,
      this.disabledColor});

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: TextFormField(
        maxLines: 1,
        enabled: enabled,
        decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 7.0),
          isDense: true,
          labelStyle: TextStyle(
            color: hintColor,
            fontSize: fontSize,
            fontFamily: 'Amagh_Demo',
          ),
        ),
        style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontFamily: 'Amagh_Demo',
        ),
        onSaved: onSaved,
        validator: validator,
        keyboardType: keyboardType,
        obscureText: showPasswordType,
        controller: controller,
        autofocus: autoFocus,
        textAlign: textAlign,
        focusNode: focusNode,
        inputFormatters: maxLength != null
            ? [LengthLimitingTextInputFormatter(maxLength)]
            : null,
      ),
      data: ThemeData(
          primaryColor: underlineColor,
          hintColor: hintColor,
          cursorColor: cursorColor,
          disabledColor: disabledColor),
    );
  }
}
