import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class OTPBoxesWidget extends StatefulWidget {
  final onSuccess;
  OTPBoxesWidget({this.onSuccess});
  @override
  _OTPBoxesWidgetState createState() => _OTPBoxesWidgetState();
  // @override
  // _OTPBoxesWidgetState createState() {
  //   return new _OTPBoxesWidgetState();
  // }
}

class _OTPBoxesWidgetState extends State<OTPBoxesWidget> {
  final FocusNode _textFocus = FocusNode();
  final _controller = TextEditingController();
  bool _loading = false, _isIcon = false, _isProcessingDone = false;
  String _errorMsg = "", _otp = "";

  _OTPBoxesWidgetState() {
    _controller.addListener(() {
      if (_isProcessingDone) {
        _isProcessingDone = false;
        _controller.clear();
        return;
      }
      setState(() {
        _otp = _controller.text;
        _isIcon = false;
        _errorMsg = "";
      });
      if (_otp.isNotEmpty && _otp.length == 4) {
       
        setState(() {
          _loading = true;
        });

        Future.delayed(Duration(seconds: 4), () {
          setState(() {
            _loading = false;
            _isProcessingDone = true;
          });
          if (_otp == '1234') {
            setState(() {
              _isIcon = true;
              _errorMsg = "";
            });
            if (widget.onSuccess != null) widget.onSuccess(_otp);
          } else {
            setState(() {
              _errorMsg = "Wrong code please try again";
              _isIcon = false;
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final password1 = AppTextField(
      underlineColor: Colors.transparent,
      hintColor: Colors.transparent,
      textColor: Colors.transparent,
      showPasswordType: true,
      maxLength: 4,
      fontSize: 19.0,
      autoFocus: true,
      cursorColor: Colors.transparent,
      controller: _controller,
      focusNode: _textFocus,
      keyboardType: TextInputType.number,
      textAlign: TextAlign.center,
    );

    final otpContainer = Container(
      child: Stack(
        children: <Widget>[
          GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(),
                  ),
                  OTPWidget(
                    isFill: (_otp?.length ?? 0) >= 1,
                    digit: (_otp?.length ?? 0) >= 1 ? _otp[0] : "",
                  ),
                  OTPWidget(
                    isFill: (_otp?.length ?? 0) >= 2,
                    digit: (_otp?.length ?? 0) >= 2 ? _otp[1] : "",
                  ),
                  OTPWidget(
                    isFill: (_otp?.length ?? 0) >= 3,
                    digit: (_otp?.length ?? 0) >= 3 ? _otp[2] : "",
                  ),
                  OTPWidget(
                    isFill: (_otp?.length ?? 0) >= 4,
                    digit: (_otp?.length ?? 0) >= 4 ? _otp[3] : "",
                  ),
                  Expanded(
                    child: Container(),
                  ),
                ],
              ),
            ),
            onTap: () {
              var p = MediaQuery.of(context).viewInsets.bottom;
              print("p->$p");
              if (p > 0) {
                FocusScope.of(context).requestFocus(new FocusNode());
              } else if (p == 0 && _textFocus.hasFocus) {
                FocusScope.of(context).requestFocus(new FocusNode());
              } else {
                FocusScope.of(context).requestFocus(_textFocus);
              }
            },
          ),
          Positioned(
            child: password1,
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
          )
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 50.0,
    );

    return Material(
      
        color: Colors.white,
        child: Padding(
            padding: EdgeInsets.only(top: 60.0, left: 20.0, right: 20.0),
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                _loading
                    ? Center(
                        child: Container(
                          width: 50.0,
                          height: 50.0,
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : otpContainer,
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 32.0),
                  child: Text(
                    _errorMsg,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                _isIcon
                    ? Container(
                        height: 40.0,
                        width: 40.0,
                        child: InkWell(
                          child: Image.asset('assets/images/download.png'),
                        ),
                      )
                    : Container()
              ],
            )));
  }
}

class OTPWidget extends StatelessWidget {
  final bool isFill;
  final String digit;

  OTPWidget({this.isFill = false, this.digit = ""});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: 50.0,
        height: 50.0,
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        padding: EdgeInsets.all(13.0),
        child: isFill
            ? Container(
                child: Text(
                  digit,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20.0, color: Colors.black87),
                ),
                height: 10.0,
                width: 10.0,
              )
            : Container(),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.blue, width: 1.0))),
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final onSaved;
  final validator;
  final Color underlineColor;
  final Color hintColor;
  final Color textColor;
  final TextInputType keyboardType;
  final bool showPasswordType;
  final bool enabled;
  final double fontSize;
  final int maxLength;
  final bool autoFocus;
  final TextAlign textAlign;
  final FocusNode focusNode;
  final Color cursorColor;
  final Color disabledColor;

  AppTextField(
      {this.hint,
      this.controller,
      this.onSaved,
      this.validator,
      this.hintColor = Colors.white,
      this.textColor = Colors.white,
      this.underlineColor = Colors.white,
      this.keyboardType = TextInputType.text,
      this.showPasswordType = false,
      this.enabled = true,
      this.autoFocus = false,
      this.fontSize = 16.0,
      this.maxLength,
      this.textAlign = TextAlign.start,
      this.focusNode,
      this.cursorColor,
      this.disabledColor});

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: TextFormField(
        maxLines: 1,
        enabled: enabled,
        decoration: InputDecoration(
          labelText: hint,
          contentPadding: EdgeInsets.all(7.0),
          isDense: true,
          labelStyle: TextStyle(
            color: hintColor,
            fontSize: fontSize,
          ),
        ),
        style: TextStyle(fontSize: fontSize, color: textColor),
        onSaved: onSaved,
        validator: validator,
        keyboardType: keyboardType,
        obscureText: showPasswordType,
        controller: controller,
        autofocus: autoFocus,
        textAlign: textAlign,
        focusNode: focusNode,
        inputFormatters: maxLength != null
            ? [LengthLimitingTextInputFormatter(maxLength)]
            : null,
      ),
      data: ThemeData(
          primaryColor: underlineColor,
          hintColor: hintColor,
          cursorColor: cursorColor,
          disabledColor: disabledColor),
    );
  }
}
