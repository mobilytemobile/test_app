import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:test_app/confirm_screen.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        theme: new ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: Dummy());
  }
}

class Dummy extends StatefulWidget {
  @override
  _DummyState createState() => _DummyState();
}

class _DummyState extends State<Dummy> {
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  String _otp = "", _errorMsg = "";
  final FocusNode _textFocus = new FocusNode();

  _OTPBoxesWidgetState() {}

  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          OTPBoxesWidget(
            onSuccess: (otp) {
              print("OTP->$otp");
            },
          )
        ],
      ),
    );
  }
}
