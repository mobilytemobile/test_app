import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart';

class Picker {
  Future<File> pickImage(ImageSource source) async {
    File file = await ImagePicker.pickImage(source: source);
    if (file != null)
      return compressImage(file);
    else
      return Future.value(null);
  }

  Future<File> compressImage(File image) async {
    //Compress Image
    Image image1 = decodeImage(image.readAsBytesSync());
    Image thumbnail = copyResize(image1, 200);

    File compressedImage = new File(image.parent.path+
        "/upload_thumbnail.jpg");
    compressedImage.writeAsBytesSync(encodeJpg(thumbnail));

    print("FILE->${compressedImage.path}");
    return compressedImage;
  }
}
